import { Context, Inject } from '@nuxt/types/app';
import { AxiosError, AxiosResponse } from 'axios';
import API, { IAPI } from '~/plugins/api/API';

// eslint-disable-next-line import/no-mutable-exports
let $API: IAPI;

export default function (context: Context, inject: Inject) {
  // console.log(process.);

  context.$axios.defaults.baseURL = 'http://localhost:3000';
  context.$axios.defaults.timeout = 15000;
  context.$axios.interceptors.response.use(
    // @ts-ignore
    function (response: AxiosResponse) {
      return response;
    },
    function (error: AxiosError) {
      console.error(error);

      return Promise.reject(error);
    },
  );
  const APIModule = API(context.$axios);
  $API = APIModule;
  inject('API', APIModule);
  return APIModule;
};

export { $API };
