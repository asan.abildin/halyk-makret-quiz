module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    'plugin:nuxt/recommended',
  ],
  plugins: [
  ],
  // add your custom rules here
  rules: {
    'import/named': 'off',
    'import/no-named-as-default-member': 'off',
    'no-debugger': 'error',
    'quote-props': [2, 'consistent'],
    'no-dupe-args': 'error',
    'no-empty': 'error',
    'no-import-assign': 'error',
    'no-irregular-whitespace': 'error',
    'no-setter-return': 'error',
    'eqeqeq': 'error',
    'no-eval': 'error',
    'no-proto': 'error',
    'no-return-await': 'error',
    'no-undef-init': 'error',
    'camelcase': 'error',
    'no-console': ['warn', { allow: ['warn', 'error'] }],
    'comma-dangle': ['error', 'always-multiline'],
    'semi': ['error', 'always', {
      omitLastInOneLineBlock: true,
    }],
    'vue/component-name-in-template-casing': ['error', 'kebab-case', {
      registeredComponentsOnly: false,
      ignores: [],
    }],
    'vue/no-v-html': 'off',
    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'interface',
        format: ['PascalCase'],
        custom: {
          regex: '^I[A-Z]',
          match: true,
        },
      },
    ],
  },
  overrides: [
    {
      files: ['*-test.js', '*.spec.js'],
      rules: {
        'no-unused-expressions': 'off',
      },
    },
  ],
  reportUnusedDisableDirectives: true,
};
