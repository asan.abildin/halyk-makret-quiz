# Тестовое задание для фронтенд разработчика
В данном проекте вы ознокомитесь с примерным стэком проекта Halyk Market и заданием для выполнения.

Для выполнения задания:
1. Склонируйте к себе проект.
1. Создайте ветку `candidate/{ваше имя}`.
1. Запустите проект.
1. Выполните задание описанное на странице "Задание".
1. Оставьте свои контактные данные в `README.MD`.
1. После выполнения задания запуште свою ветку на gitlab.
1. Оставьте merge request на ветку `main`.
1. Убедитесь что merge request прошёл тесты. Merge request не прошедший тесты рассматриваться не будет!
1. Следите за комментариями в gitlab-е.

По вопросам можно обращаться ко мне в [телеграм](https://t.me/lcripl).
## Запуск проекта

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# для запуска unit тестов
$ npm run test

# для запуска eslint
$ npm run lint
```

[документация Nuxt.js](https://nuxtjs.org).

## Контактные данные
Имя:  
Фамилия:  
Email:  
Телефон:  